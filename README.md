# Chrome extension POC

## Background

Bin Wang:

QTrade Site Compilation

1. I heard from Tony that the people working within QTrade has a list of websites (30+ in total) where they went out to collect information. (For example, that I am one of them, and give a part/quote, I will need to go to the xxx.com, login using my user name and password, do a search, look at the pricing, and compare with another site, abc.com, xyz.com, ..etc). 

2. I need your help to develop a Chrome/Firefox Plugin where demo the underlying idea of this can be automated via scripting. (click a button, type in a part number and the plugin will use stored credentials to log into necessary websites and collect certain information and maybe generate a output. For the sake of demo, maybe you can find a few dummy B2C websites that has credentials and prove out the idea)

## Description

Chrome extension for crawling part number search results on findchips.com.

User specifies the part number to search for on the pop up and starts search.

The application injects a content script to perform the necessary actions to search and crawl the data.

Content script actions are divided into 3 states: login, search and results.

In the search step, the application goes to findchips.com, waits till page is fully loaded (DOM ready) and automatically performs the search.

In the results step, injected content script collects and converts the html information (results of the performed search) into an array of JSON objects.

**Please note login step is not used at findchips.com.**

When the results step is ready, the application creates a sample report on a new page.

## How to use
1. Visit chrome://extensions in your browser (or open up the Chrome menu by clicking the icon to the far right of the Omnibox. The menu's icon is three horizontal bars. and select Extensions under the More Tools menu to get to the same place).

2. Ensure that the Developer mode checkbox in the top right-hand corner is checked.

3. Click Load unpacked extension� to pop up a file-selection dialog.

4. Navigate to the directory in which your extension files live, and select it.

Source: https://developer.chrome.com/extensions/getstarted

## More information on chrome extension development
https://developer.chrome.com/extensions

