// Application configuration object: contains steps and step details which is neccessary to crawl the data for each page
var CONFIG = { 
                "findchips": [
                     { site: 'https://www.findchips.com/', id: 'search', state: 'search', property: 'id', multiple: false, searchAddition: false },
                     { site: 'https://www.findchips.com/search/', id: 'distributor-results', state: 'result', property: 'class', multiple: true, searchAddition: true },
                     { site: 'https://www.findchips.com/signin', id: 'distributor-results', state: 'result', property: 'class', multiple: true, searchAddition: false }
                ]
             }

/**
  * @desc function to find the object of the current state from the CONFIG object
  *
  * @param {string} current_site - url of the current site
  * @param {Array of objects} config_page - list of states (from CONFIG object) of the current site to crawl
  * @param {string} url_addition -  parameter of the current url in case it needs
  *
  * @return {Object} - the object of the current state
*/
var get_state = function(current_site, config_page, url_addition) {
    for (var i = 0; i < config_page.length; i++) {
        // if search addition (e.g.: mpn to search), than concat to url
        if (config_page[i].searchAddition) 
            config_page[i].site = config_page[i].site + url_addition;
          
        if(current_site == config_page[i].site)
            return config_page[i];
    }    
}


/**
  * @desc function to perform search state actions: finds and fill input element for search and submit the search 
  *
  * @param {object} config_current - confing JSON object of the current state of curent page
  * @param {string} element_to_search - element (mpn) to be searched by the application
*/
var state_action_search = function(config_current, element_to_search) { 
    
    var search_element = get_element(config_current);
    search_element[0].value = element_to_search;
    search_element.submit();
}

/**
  * @desc function to find and return html element by id or class
  *
  * @param {object} config_current - confing JSON object of the current state of curent page
  * 
  * @return {Object} - specific html element of target website
*/
var get_element = function(config_current) {
    
  if(config_current.property == 'id') {
      return document.getElementById(config_current.id);
  }
  
  if(config_current.property == 'class') {
      return document.getElementsByClassName(config_current.id);
  }
  
  return null;
  
}


/**
  * @desc function to indentify which currency the price
  *
  * @param {string} price - price string with currency sign retrived from html
  * 
  * @return {string} - currency sign
*/
var get_currency = function(price) {
  //TODO: implement in application config, to make processing generic
  if (price.indexOf("$") !=-1) { return "$"; }
  if (price.indexOf("£") !=-1) { return "£"; }
  if (price.indexOf("€") !=-1) { return "€"; }
}

/**
  * @desc function to create an array of objects from distributors html table data
  *
  * @param {HTMLelement} table - html table element of current distributor
  *
  * @return {array} - array of objects (e.g.: { mpn: 'mpn', mfr : 'mfr', stock: 5, prices : [ { qty: 5, price: 10.00, currency: '$'} ] } )
*/
var processor_element = function(table) {
  //TODO: implement in application config, to make processing generic
  var results_list = []
  for (var i = 1; i < table.length-1; i = i+2) {
      // get current mpn 
      var mpn = table[i].childNodes[1].innerText.trim();
      // get current manufacturer
      var mfr = table[i].childNodes[3].innerText.trim();
      // get current stock information
      var stock = table[i].childNodes[7].innerText.trim();
      // iterate through price information if any
      if (table[i].childNodes[9].childNodes.length > 1) {
          // get price list
          var prices = table[i].childNodes[9].childNodes[1].childNodes;
          var price_list = [];
          // iterate through price list
          for (var j = 1; j < prices.length-1; j = j+2) {
              // get price line
              var price_line = prices[j].innerText.trim();
              // get currency of current price line
              var currency = get_currency(price_line);
              // get where to split string since it contains currency sign
              var separator = price_line.indexOf(currency);
              // create price list element
              price_list.push( { qty : price_line.substring(0, separator), price : parseFloat(price_line.substring(separator+1)), currency: price_line.substring(separator, separator+1)  } );
          }
          results_list.push( { mpn: mpn, mfr : mfr, stock: stock, prices : price_list } );
      }
      
  }
  
  return results_list;
}

/**
  * @desc function to create an array of objects from the collected HTML elements
  *
  * @param {array} elements - list of html elements
  *
  * @return {array} - array of objects (e.g.: { dist : 'distributor', data: processor_element(table) } )
*/
var processor = function(elements) {
    //TODO: implement in application config, to make processing generic
    var results = []
    for (var i = 0; i < elements.length; i++) {
        // get distributor
        var dist = elements[i].childNodes[5].childNodes[1].childNodes[2].textContent.trim()
        // get html table data of current distributor
        var table = elements[i].childNodes[7].childNodes[3].childNodes;
        // create list of objects from table
        var processed = processor_element(table);
        // add element to distributor data object list
        results.push( { dist : dist, data: processed } );
    }
  
    return results;
}

/**
  * @desc function to click on all "see more" <a href> html elements to load more prices
  *
  * @param {array} a_list - list of all the <a href> html elements on page
*/
var show_all_price = function(a_list)  { 
    for (var i = 0; i < a_list.length; i++) {
      // find proper elements: if it has the text 'See More' than click on it
      if (a_list[i].innerText == 'See More') {
          a_list[i].click();
      }
    }
}



/**
  * @desc function to perform search state actions: send crawled data to application background for processing
  *
  * @param {object} config_current - confing JSON object of the current state of curent page
*/
var state_action_result = function(config_current) { 
    //TODO: make it generic, now it performs only on findchips
    var result_element = get_element(config_current);
    
    // retrieve hidden prices by clicking on corresponding 
    var price_a_list = document.getElementsByTagName('a');
    show_all_price( Array.prototype.slice.call(price_a_list) );

    var a = Array.prototype.slice.call(result_element)
    // create an array of object from html elements for results
    var res = processor(a);
    // send results to background to evaulate it
    send_data("results", res);
}

/**
  * @desc function to perform login state actions: fills user name and password and submit the form
  *
  * @param {object} config_current - confing JSON object of the current state of curent page
*/
var state_action_login = function(config_current) { 
    //TODO: implement
    return 0;
}

/**
  * @desc function to send data to application background for processing
  *
  * @param {string} message - message to interprent what to do with the sent data
  * @param {object} data - data to be sent
  *
  * @return {function} - chrome.runtime.sendMessage(object)
*/
var send_data = function(message, data) {
    return chrome.runtime.sendMessage({message: message, data: data});
}


/**
  * @desc function to decide what action to do on page
  *       controll sequence checks the current state
  *       a state can be login, search or result
  *       states and action are refered to page urls
  * 
  * @param {object} config_current - confing JSON object of the current state of curent page
  * @param {string} element_to_search - element (mpn) to be searched by the application
*/
var state_action_do = function(config_current, element_to_search) {
  
    switch(config_current.state) {
      case 'search':
          state_action_search(config_current, element_to_search);
          break;
      case 'result':
          state_action_result(config_current);
          break;
      case 'login':
          break;
      default:
          break;
          
    }
  
}

/**
  * @desc function to get url of the current page and return state config from application CONFIG
  *      
  * @param {string} id_current - id of the current website in the application CONFIG
  * @param {string} element_to_search - element (mpn) to be searched by the application
  *
  * @return {function} - get_state(currentLocation, CONFIG[id_current], element_to_search)
*/
var get_state_current = function(id_current, element_to_search) {
    // Getting the url of the current page
    var currentLocation = window.location.origin + window.location.pathname;
    // return current state's configuration object from CONFIG 
    return get_state(currentLocation, CONFIG[id_current], element_to_search);
}

/**
  * @desc add event listener to page on load even
  *       notifies background that the page is loaded
  *          
*/
window.addEventListener('load', function() {
    send_data('page_loaded', null);
})

/**
  * @desc event listener to listen for incoming messages
  *       this function controlls the process of the application
  *       checks the state of the current page and decides which actions to perform via state_action_do function
  *       first it waits to get the part number to search for
  *       message passing structure is neccessary to comunicate between content script and the popup
  *       
*/
var onMessageListener = function(request, sender, sendResponse) {
    if(request.message == 'page_loaded_response') {
        search_element = request.data;
        var state = get_state_current('findchips', search_element)
        state_action_do(state, search_element);
    }
}
//add event listener
chrome.runtime.onMessage.addListener(onMessageListener);