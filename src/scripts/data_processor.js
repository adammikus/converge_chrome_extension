/**
  * @desc function to find top 3 prices of the searched component
  *
  * @param {array} data_array_min_prices - array of objects containing the minimum prices of distributors
  * 
  * @return {array} - 3 element array of objects
*/
var find_top_3_prices = function(data_array_min_prices) {
    return data_array_min_prices.sort( function(a, b) { return a.min_price - b.min_price } ).slice(0, 3);
}


/**
  * @desc function to find minimum prices of distributors crawled from findchips.com
  *
  * @param {array} data_array - array of objects containing the crawled data from findchips.com
  * 
  * @return {array} - array of objects (e.g.: { dist: 'distributor', mpn: 'mpn', mfr: 'manufacturer', min_price: 10.00, currency: '$' } )
*/
var find_min_price_of_distributors = function(data_array) {
    // if no results for search, return
    if(data_array.length == 0) {
        return 'No data for this part';
    }
    
    var min_prices_distributor = [];
    for (var i = 0; i < data_array.length; i++) {
        // get distributor data
        current_distributor = data_array[i];
        var min_price;
        var mpn;
        var mfr;  
        var currency;  
        
        // find minimum prices
        for (var j = 0; j < current_distributor.data.length; j++) {
            min = Math.min.apply(Math, current_distributor.data[j].prices.map( function(o) { return o.price; } ) )
            
            if (j == 0) { min_price = min; }
                
            if (min < min_price) { 
                min_price = min;
                mpn = current_distributor.data[j].mpn;
                mfr = current_distributor.data[j].mfr;
                currency = current_distributor.data[j].prices[0].currency;
            }

        }
        // create object for distributor with minimum price:
        // contains information about distributors name, the searched mpn, manufacturer of the mpn, minimum price and its currency
        min_prices_distributor.push( { dist: current_distributor.dist, mpn: mpn, mfr: mfr, min_price: min_price, currency: currency } );
    }
    return min_prices_distributor;
}

/**
  * @desc function to create html table of the report 
  *
  * @param {array} table_headers - array of strings containing the labels of the table headers in order
  * @param {array} data - array of objects containing the data to present in the table rows
  * @param {string} title - title of the table (report)
  * 
*/
var add_ui_table = function(table_headers, data, title) {
    // Create title
    var heading = document.createElement("h3");
    heading.innerHTML = title;
    document.body.appendChild(heading);
    // Create table
    var table = document.createElement("table");
    // Create header of the table
    var header = table.createTHead();
    var header_row = header.insertRow(0);  
    // Add header cells
    for (var i = 0; i < table_headers.length; i++) {
        var header_cell = header_row.insertCell(i);
        header_cell.innerHTML = '<b>' + table_headers[i] + '</b>';
    }
    // row elements to table
    var body = document.createElement("tbody");
    for (var j = 0; j < data.length; j++) {
        var row = body.insertRow(j);
        
        Object.keys(data[j]).forEach(function(key,index) {
            var cell = row.insertCell(index);
            cell.innerHTML = data[j][key];
        });

    }
    
    table.appendChild(body);
    // add table to page
    document.body.appendChild(table);
}

/**
  * @desc event listener to listen for incoming messages
  *       creates report after contect script sent the crawled data
  *       
*/
var onMessageListener = function(request, sender, sendResponse) {
    if(request.message == 'result_page') {
        // get minimum prices of distributors
        var mins = find_min_price_of_distributors(request.data);
        if (mins != 'No data for this part') {
            // get top 3 distributor with prices
            var best_3 = find_top_3_prices(mins);
            // create report
            add_ui_table(['Distributor', 'MPN', 'Manufacturer', 'Best Price', 'Currency'], mins, 'Minimum prices of distributor');
            add_ui_table(['Distributor', 'MPN', 'Manufacturer', 'Best Price', 'Currency'], best_3, 'Top 3 distributors by price');
        }
    }
   

}
chrome.runtime.onMessage.addListener(onMessageListener);