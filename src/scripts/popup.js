/**
  * @desc event listerer to watch incoming messages for content script
  *       sends the results of crawling to the report page
  *       creates communication between application storage and content script (content script can not access application storage)
  *       
*/
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    // if it receives the results of crawling, than it opens a new tab for results page and sends the crawled data to the page
    if (request.message == "results") {
        var myData = request.data;
        // opens a new tab for results page 
        chrome.tabs.create( {url: chrome.extension.getURL('src/results.html')}, function() {
            // sends the crawled data with 5 sec timeout
            setTimeout( function() { chrome.runtime.sendMessage({message: "result_page", data: myData}); } , 5000 );
        });
    }
    // if page to crawl loaded, than it sends the mpn to search to the content script
    if(request.message == 'page_loaded') {
        // get mpn to search from application storage
        chrome.storage.sync.get(null, function(storage_data) { 
            chrome.tabs.sendMessage( sender.tab.id, { message: "page_loaded_response", data: storage_data.search } );
        });
    }
  
    return true; 
});

// Add event listerer to inject content script to target site
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if (changeInfo.status == 'complete') {
        // Execute content_script when the page is fully (DOM) ready
        chrome.tabs.executeScript(null, {file: 'src/content_script.js'});
    }
});

/**
  * @desc function to open a new page in a new tab
  *
  * @param {string} url - url of the current site
  *
  * @return {function} - chrome.tabs.create(object)
*/
var new_page = function(url) {
    return chrome.tabs.create({url: url});
};

/**
  * @desc add ready event listener to document
  *       using jQuery functions
  *       add on click event listners to ui buttons of the extension
  *       
*/
$(document).ready(function() {
    //add on click event listener to search button, 
    //put value to search into application storage, opens a new page and starts search
    $("#search").click(function() {
        var part_number = document.getElementById("mpn_search").value;
        chrome.storage.sync.set( {search : part_number }, function() {
            new_page('https://www.findchips.com/');
        });
    });
});